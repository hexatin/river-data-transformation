import csv
import math
from collections import OrderedDict
import argparse
# This will convert the data in the file "historical simulation" to the .rih format as shown

# 1. Read in the data from historical simulation
# 2. Put the historical sim data in some sort of logical data structure
# 3. Print it all in the output format (one big for loop) with all the empty rows included

# gross global variables
initial_year = 1950
final_year = 2012


def get_node_data_by_month(historical_data, node, year, month):
    cur = historical_data[node]
    # cur should be a list of data points by zero-based time period

    # if year is same as intial, this should be 0
    # this way the month number can be detrmined by multiplying
    # by this year multiplier
    # adj = base_month_number + (mul * months_in_year)
    # ex. january 1950 is  1 + (0 * 12) = 1
    #     decembe 1950 is 12 + (0 * 12) = 12
    #     january 1951 is  1 + (1 * 12) = 13
    #     decembe 1951 is 12 + (1 * 12) = 24
    year_multiplier = year - initial_year

    month_number = month + (year_multiplier * 12)

    # because the list's time periods are zero-based
    index = month_number - 1

    #print(f'found index to be {index} for {month},{year}')

    if(index < len(cur)):
        #print(f'cur[index] = {cur[index]}')
        return cur[index]
    else:
        return 'null'



# We gonna do this TDD-ish style
# Start with the final method, and go backwards
def print_to_rih(historical_data, out_path):
    with open(out_path, 'w') as outfile:
        res = ''
        res += '    1/1950  -     12/2012 ACFT  CYR\n'

        # for each year
        for year in range(initial_year, final_year+1):
            # for each of the nodes (06754000, Ab_Kersey, etc)
            # historical_data is a dict where the keys are node names
            # and the value is a list of data points at each time period
            for node in historical_data:
                head = '{0} {1}'.format(year, node)
                line = '{0: <17}'.format(head)
                # for each month of the current year
                for month in range(1, 12+1):
                    # print
                    #print(f'want {node} at {month},{year}')
                    line += ' {0: >6}.'.format(
                        get_node_data_by_month(historical_data, node, year, month)
                    )
                line += '\n'
                res += line

        outfile.write(res)

# The historical_data is an ordered dict
# that means you can add data to the things that are in there and they will preserve order
def read_historical_data(historical_sim_path):
    # These are the nodes that have data points in the file
    nonzero_nodes = [
        '06754000',
        '06758500',
        '06759910',
        '06764000',
        'Ab_Kersey',
        'Latham_I'
    ]

    # create the historical_data ordereddict
    all_nodes = [
        '06754000',
        '06758500',
        '06759910',
        '06764000',
        'Ab_Kersey',
        'BijouCk_O',
        'Bijou_O',
        'Jackson_O',
        'Julesburg_O',
        'KiowaCk_O',
        'Latham_I',
        'LostCk_O',
        'Prewitt_O',
        'Riverside_O',
        'Sterling_O'
    ]
    historical_data = OrderedDict({})
    for node in all_nodes:
        historical_data[node] = []

    # read the file and add the data in line-by-line
    with open(historical_sim_path, 'r', newline='') as infile:
        reader = csv.reader(infile)

        # skip the first line because it's just a header
        reader.__next__()

        for line in reader:
            # each line is a list of data points at a single time period
            # the first column is the time period
            # the columns after that are for each of the six nodes in question
            # so the first column doesn't really matter for us
            for col in range(1,len(line)):
                # col 1 is actually node 0 of the nonzero nodes
                node_name = nonzero_nodes[col-1]
                historical_data[node_name].append(int(float(line[col])))

    final_year = initial_year + ((len(historical_data['06754000']) // 12) - 1)

    return historical_data

def add_zeroes(historical_data):
    zero_nodes = [
        'BijouCk_O',
        'Bijou_O',
        'Jackson_O',
        'Julesburg_O',
        'KiowaCk_O',
        'LostCk_O',
        'Prewitt_O',
        'Riverside_O',
        'Sterling_O'
    ]
    num_months = ((final_year+1) - initial_year) * 12
    for node in zero_nodes:
        historical_data[node] = [0]*num_months
    
    return historical_data

def main():
    parser = argparse.ArgumentParser(description="Converts flow data files that were created for this project into a format understandable by StateMod.")

    parser.add_argument('[csv path]')
    parser.add_argument('[output path]')

    args = parser.parse_args()

    #historical_sim_path = 'input-files/historical-sim-to-rih-files/historical_simulation_1.csv'

    historical_sim_path = vars(args)['[csv path]']
    output_path = vars(args)['[output path]']
    
    # this is just sites 1-6 at each time period
    historical_data = read_historical_data(historical_sim_path)
    #print(historical_data)

    # this is the data for ALL nodes at each time period, even the ones that are just zero
    historical_data = add_zeroes(historical_data)
    #print(historical_data)

    print_to_rih(historical_data, output_path)

main()
