import re
import csv
import argparse
from shutil import copyfile

############################
#         GLOBALS
############################
exceptions = [21, 41, 42, 43, 44, 46, 47, 48]

# april - october
# january is 1, december is 12
exception_months = [4, 5, 6, 7, 8, 9, 10]

scenario = None


###########################
#        OBJECTS
############################
class TimePeriod:
    def __init__(self, zero_based_time):
        # time period is initialized with its 0-based time
        self.zero_based_time = zero_based_time
        self.one_based_time = self.zero_based_time + 1

    def previous_time(self):
        # returns the TimePeriod object for the time period right before this one
        # by subtracting one from its zero_based_time and using that to make a new
        # TimePeriod
        return TimePeriod(self.zero_based_time - 1)


# Reads from diversion summary to obtain the IDs of each structure in the summary
# Returns a list of strings, each string being an ID of a structure
def get_structure_ids(diversion_summary_path):

    # Pre-compile regex for use later
    struct_id_prog = re.compile(r'STRUCTURE ID.+:\s+(\S+)\s+')
    river_location_id_prog = re.compile(r'RIVER LOCATION - FROM.+:\s+(\S+)\s+')
    page_splitter_prog = re.compile(r'PAGE NO.\s*(\S+)')

    list_of_page_dicts = []

    with open(diversion_summary_path, 'r') as diversion_summary:
        div_sum_text = diversion_summary.read()

        # Split the diversion summary into pages
        div_sum_pages = page_splitter_prog.split(div_sum_text)

        for page in div_sum_pages:
            page_dict = {}

            # Search the page for struct_id and river_location_id
            struct_id = struct_id_prog.search(page)
            river_loc_id = river_location_id_prog.search(page)
            if (struct_id):
                page_dict['structure_id'] = struct_id.group(1)
            if (river_loc_id):
                page_dict['river_location_id'] = river_loc_id.group(1)

            # store the raw page for more processing later
            page_dict['raw'] = page

            list_of_page_dicts.append(page_dict)

    return list_of_page_dicts


def get_res_sum_pages(res_sum_path):
    """Reads a reservoir summary file into a list of dicts, wherein each dict
    contains the reservoir's ID and the raw data from its page.
    """

    res_id_prog = re.compile(r'RESERVOIR ID.+:\s+(\S+)\s+')
    page_splitter_prog = re.compile(r'PAGE NO.\s*(\S+)')

    list_of_page_dicts = []

    with open(res_sum_path, 'r') as reservoir_summary:
        res_sum_text = reservoir_summary.read()

        res_sum_pages = page_splitter_prog.split(res_sum_text)

        for page in res_sum_pages:
            page_dict = {}

            # Search the page for reservoir id
            res_id = res_id_prog.search(page)
            if (res_id):
                page_dict['reservoir_id'] = res_id.group(1)
                page_dict['raw'] = page
                list_of_page_dicts.append(page_dict)

    return list_of_page_dicts


# Reads from specified csv file to find 3 things:
# 1. An object's 'index'
# 2. An object's 'id'
# 3. An object's 'name'
# Returns a list of OrderedDicts, each one containing these 3 properties for a certain object.
def make_lookup_table(lookup_csv_path):
    table = []
    with open(lookup_csv_path, 'r', newline='') as lookup_table:
        reader = csv.DictReader(lookup_table)
        for row in reader:
            table.append(row)
    #print(table)
    return table


def make_penalty_sheet(penalty_file):
    # parse the csv into a dict
    # dict: node -> penalty
    sheet = {}
    with open(penalty_file, 'r', newline='') as penalty_sheet:
        reader = csv.reader(penalty_sheet)
        reader.__next__()  # trash the header row
        for row in reader:
            # each row is a list with [node, penalty]
            sheet[int(row[0])] = row[1]

    return sheet


def find_relevant_summary_pages(input_pages, lookup_table):
    rslt = []
    for page in input_pages:
        for validator in lookup_table:
            # If the structure id is valid,
            # or the structure id without its first digit is valid,
            # or the river id is valid,
            # or the river id without its first digit is valid,
            # or the river id is "Ab_Kersey"
            # as long as that key exists in the dictionary.
            has_struct_id = 'structure_id' in page
            full_struct_id_matches      = has_struct_id and page['structure_id'] == validator['id']
            partial_struct_id_matches   = has_struct_id and page['structure_id'][1:] == validator['id']
            relevant_struct_id          = has_struct_id and ( full_struct_id_matches or partial_struct_id_matches )

            has_river_id = 'river_location_id' in page
            full_river_id_matches       = has_river_id and page['river_location_id'] == validator['id']
            partial_river_id_matches    = has_river_id and page['river_location_id'][1:] == validator['id']
            relevant_river_id           = has_river_id and ( full_river_id_matches or partial_river_id_matches )

            #is_ab_kersey = (has_river_id and page['river_location_id'] == 'Ab_Kersey') # special side-case for ab kersey

            if ((relevant_struct_id) or (relevant_river_id)):
                page['index'] = int(validator['index'])
                rslt.append(page)
                #print("{} == {}".format(page, validator['id']))
                validator['found'] = True
                break  # we found it so stop looping through pages

    for validator in lookup_table:
        if ('found' not in validator
                or ('found' in validator and not validator['found'])):
            print('Structure with ID {} not found in Diversion Summary'.format(
                validator['id']))
    return rslt


def find_relevant_res_sum_pages(input_pages, relevant_reservoirs_map):
    # TODO if the value associated with the key is 'zeroes', then generate all zeroes
    # instead of reading from the file
    rslt = []

    # the map has IDs as keys and indexes as values
    for validator in relevant_reservoirs_map:
        if (validator == 'zeroes'):
            # special case
            # edge case for the reservoir that is unused/all zeroes
            zero_reservoir_index = relevant_reservoirs_map['zeroes']
            zero_page = {}
            zero_page['index'] = zero_reservoir_index
            zero_page['reservoir_id'] = 'zeroes'
            relevant_reservoirs_map['zeroes'] = 'found'
            rslt.append(zero_page)
            # this one's gonna be kinda strange because it has no raw data associated with it,
            # it should just return zero whenever asked for data
        else:
            for page in input_pages:
                if (('reservoir_id' in page
                     and page['reservoir_id'] == validator)
                        or ('reservoir_id' in page
                            and page['reservoir_id'][1:] == validator)):
                    page['index'] = relevant_reservoirs_map[validator]
                    relevant_reservoirs_map[validator] = 'found'
                    rslt.append(page)
                    break

    for validator in relevant_reservoirs_map:
        if (relevant_reservoirs_map[validator] != 'found'):
            print(
                f'Reservoir with ID {validator} not found in Reservoir Summary!'
            )

    return rslt


def print_diversion_summary_pages(diversion_summary_pages):
    print(f"Diversion Summary Pages: {len(diversion_summary_pages)}")
    for page in diversion_summary_pages:
        if ('structure_id' in page and 'river_location_id' in page):
            print(
                f"-----\nSTRUCTURE: {page['structure_id']}\nRIVER: {page['river_location_id']}\nINDEX: {page['index']}\nTOTAL SHORT: {page['total_short']}\nAVAIL FLOW: {page['avail_flow']}"
            )


def add_data_to_summary_pages(relevant_summary_pages):

    table_splitter_prog = re.compile(r'.*Shortage.*\n.*\n.*\n.*\n.*\n.*\n_+.*')

    for page in relevant_summary_pages:
        # Holds total shortage data for this page, in order by time period
        page['total_short'] = []

        # Same but for available flow
        page['avail_flow'] = []

        # Same but for river inflow
        page['river_inflow'] = []

        # Same but for river outflow
        page['river_outflow'] = []

        # now split each page into the yearly data-reporting tables
        page_tables = table_splitter_prog.split(page['raw'])
        for table in page_tables:
            # now split every table into lines
            table_lines = table.split('\n')

            # we only want the entries in the list that have more than 12 lines (i.e. are an actual table)
            if (len(table_lines) >= 12):
                # we only want the first 12 lines
                # start at 1 though because the line at index 0 is garbage
                for i in range(1, 13):
                    line = table_lines[i]

                    # split each line by periods, then by spaces
                    tokens = list(map(lambda x: x.split(), line.split('.')))

                    # this gives you a list of lists, so flatten it
                    tokens = [item for sublist in tokens for item in sublist]

                    # now extract the tokens that we want
                    if (len(tokens) >= 17):

                        #ts = tokens[17].split('.')[0]
                        #af = tokens[32].split('.')[0]
                        #ri = tokens[28].split('.')[0]
                        #ro = tokens[31].split('.')[0]
                        ts = tokens[17]
                        af = tokens[32]
                        ri = tokens[28]
                        ro = tokens[31]
                        if (ts != 'NA'):
                            page['total_short'].append(int(ts))
                        if (af != 'NA'):
                            page['avail_flow'].append(int(af))
                        else:
                            print('Encountered NA at line',
                                  len(page['avail_flow']), 'of',
                                  page['structure_id'])
                            return
                        if (ri != 'NA'):
                            page['river_inflow'].append(int(ri))
                        if (ro != 'NA'):
                            page['river_outflow'].append(int(ro))
                    else:
                        continue
            else:
                continue

    return relevant_summary_pages


def add_data_to_res_sum_pages(relevant_res_sum_pages, time_periods):
    table_splitter_prog = re.compile(
        r'.*From Storage to.*\n.*\n.*\n.*\n.*\n.*\n_+.*')
    for page in relevant_res_sum_pages:
        page['total_supply'] = []
        page['total_release'] = []

        try:
            if (page['reservoir_id'] == 'zeroes'):
                #print zeroes instead
                for i in range(len(time_periods)):
                    page['total_supply'].append(0)
                    page['total_release'].append(0)
                continue
        except KeyError:
            print(f'my name is {page}, and I don\'t have an id')

        page_tables = table_splitter_prog.split(page['raw'])
        for table in page_tables:
            table_lines = table.split('\n')
            if (len(table_lines) >= 12):
                for i in range(1, 13):
                    line = table_lines[i]
                    tokens = line.split()
                    if (len(tokens) >= 17):
                        page['total_supply'].append(
                            int(tokens[12].split('.')[0]))
                        page['total_release'].append(
                            int(tokens[16].split('.')[0]))
                    else:
                        continue
            else:
                continue
    return relevant_res_sum_pages


# Consumes: TimePeriod object
# Returns: relative month of the year
# january is 1, december is 12
def to_month(time_period):
    t = time_period.one_based_time
    rem = t % 12

    # rem is the number of the month in all cases except december
    # in which case rem is 0 and the month should actually be 12
    if (rem == 0):
        return 12
    else:
        return rem


# at each time period, list nodes that have supply
# and the scenario (for the moment, always 1)
# and what the supply is (avail_flow(n) - avail_flow(n-1))
# return that as a list of tuples
def calc_chat_param(pages, time_periods):
    # loop through time periods
    # loop through nodes
    # calculate supply
    # if nonzero add it to the list

    chats = []


    for t in time_periods:
        # special case: pages[0] has nothing before it so if its avail_flow
        # is nonzero just add it
        # and don't include it in the for loop
        if (pages[0]['avail_flow'][t.zero_based_time] > 0):
            chats.append({
                'tp': t,
                'scenario': scenario,
                'node': pages[0]['index'],
                'data': pages[0]['avail_flow'][t.zero_based_time]
            })

        for i in range(1, len(pages)):
            supply = pages[i]['avail_flow'][t.zero_based_time] - pages[
                i - 1]['avail_flow'][t.zero_based_time]
            if (supply > 0):
                chats.append({
                    'tp': t,
                    'scenario': scenario,
                    'node': pages[i]['index'],
                    'data': supply
                })

    return chats


# at each time period, list nodes that have demand
# and the scenario (for the moment, always 1)
# and what the demand is (total_short(n))
# return that as a list of tuples
def calc_de_param(pages, time_periods):
    # loop through all the time periods
    # loop through nodes
    # get total short
    # if nonzero, add it to the list
    # if one of the exceptions, add it to the list
    # make sure exceptions don't get added twice
    # also at the end of every time period loop, node "50" MUST be added

    des = []


    for t in time_periods:
        for page in pages:
            demand = page['total_short'][t.zero_based_time]

            # is the page one of the ones that is always "in demand" and is it the right months?
            isException = int(page['index']) in exceptions and to_month(
                t) in exception_months

            # if the page has an actual demand or it's just one of the ones that's always there
            if (demand > 0 or isException):
                des.append({
                    'tp': t,
                    'scenario': scenario,
                    'node': page['index'],
                    'data': demand
                })

        # the omni-present node 50 with data 0
        des.append({'tp': t, 'node': 50, 'scenario': scenario, 'data': 0})

    return des


# loops through each demand node d
# at each node looks for river inflow from diversion summary
# returns tuple (timeperiod, node, scenario, inflow)
def calc_vplus_param(pages, d_set):
    ans = []

    for key in d_set:
        for node in d_set[key]:
            if (node == 50):
                inflow = 0
            else:
                page = get_div_sum_page_by_index(pages, node)
                inflow = page['river_inflow'][key[0].zero_based_time]

            ans.append({
                'tp': key[0],
                'node': node,
                'scenario': scenario,
                'data': inflow
            })

    return ans


# same thing as vplus but a different div sum column
# river outflow
# returns tuple (timeperiod, node, scenario, inflow)
def calc_vminus_param(pages, s_set):
    ans = []


    for key in s_set:
        for node in s_set[key]:
            if (node == 50):
                outflow = 0
            else:
                page = get_div_sum_page_by_index(pages, node)
                outflow = page['river_outflow'][key[0].zero_based_time]

            ans.append({
                'tp': key[0],
                'node': node,
                'scenario': scenario,
                'data': outflow
            })

    return ans


def calc_vd_param(reservoir_summary_pages, time_periods):
    vds = []


    for tp in time_periods:
        for page in reservoir_summary_pages:
            sup = page['total_supply'][tp.zero_based_time]
            vds.append((page['index'], tp, scenario, sup))

    return vds


def calc_ve_param(reservoir_summary_pages, time_periods):
    ves = []


    for tp in time_periods:
        for page in reservoir_summary_pages:
            rel = page['total_release'][tp.zero_based_time]
            ves.append((page['index'], tp, scenario, rel))

    return ves


def get_div_sum_page_by_index(div_sum_pages, index):
    possibles = list(
        filter(lambda page: page['index'] == index, div_sum_pages))
    if len(possibles) > 1: print('Warning: more than 1 page meets criteria!')
    return possibles[0]


def calc_pe_param(penalty_sheet, de):
    pes = []

    # row should be a tuple (tp, node, scenario, demand)
    for row in de:
        node = row['node']

        # penalty is actually based on the node
        try:  # not all nodes are represented in the sheet
            penalty = penalty_sheet[node]
        except KeyError:
            penalty = 0

        # return dict (tp, scenario, node, penalty)
        pes.append({
            'tp': row['tp'],
            'scenario': scenario,
            'node': row['node'],
            'data': penalty
        })

    return pes


# loop through everything in chat
# look at the time period
# at ans[t] make a list of the nodes that
# are in the chat list at that t
def calc_s_set(chat, time_periods):
    # every time period must be represented even if it is empty
    # this is a {dict->{dict->list}}, {tp,scenario}:[data]
    ans = {}

    # populate ans with all of the possible time period objects
    for t in time_periods:
        ans[(t,scenario)] = []

    # put the things in the places
    for c in chat:
        time = c['tp']
        ans[(time,scenario)].append(c['node'])

    return ans


def calc_d_set(de, time_periods):
    # this is a {dict->{dict->list}}, {tp,scenario}:[data]
    ans = {}

    for t in time_periods:
        ans[(t,scenario)] = []

    # the d set is a subset of the de param
    # loop through de
    for d in de:
        # get time period from de
        time = d['tp']

        # add node index to list at that time
        ans[(time,scenario)].append(d['node'])

    return ans


# Create the N[] parameter
# N[] set of all demand nodes that are in section 64 and hold water rights junior to the compact
# in other words, the exception nodes from D[]
def calc_n_set(time_periods):
    ans = {}

    for t in time_periods:
        # Since the exceptions are always in D[] during the exception months,
        # if t is an exception month, all that will be listed is the exceptions
        # and otherwise nothing since N[] only lists the exceptions
        if (to_month(t) in exception_months):
            ans[(t,scenario)] = exceptions
        else:
            ans[(t,scenario)] = []

    return ans


def set_to_string(st, set_name):
    """Converts a set such as D[] or S[] to the properly formatted string."""
    ans = ''
    for key in st:
        line = f'set {set_name}[{key[0].one_based_time},{key[1]}] := '
        for node in st[key]:
            line += str(node) + ' '
        line += ';\n'
        ans += line
    return ans


# DEPRECATED
#def param_to_string(param, param_name):
#    param_string = f'param {param_name}:=\n'
#
#    for tup in param:
#        param_string += f'{tup[0].one_based_time} {tup[1]} {tup[2]} {tup[3]}\n'
#    param_string = param_string[:-1]  # no trailing newline
#    param_string += ';'
#
#    return param_string


def dict_param_to_string(param, param_name):
    param_string = f'param {param_name}:=\n'

    for row in param:
        param_string += f'{row["tp"].one_based_time} {row["scenario"]} {row["node"]} {row["data"]}\n'
    param_string = param_string[:-1] # no trailing newline
    param_string += ';'

    return param_string


def reservoir_param_to_string(param, param_name):
    param_string = f'param {param_name}:=\n'

    for tup in param:
        param_string += f'{tup[0]} {tup[1].one_based_time} {tup[2]} {tup[3]}\n'
    param_string = param_string[:-1]  # no trailing newline
    param_string += ';'

    return param_string


def print_to_file(time_periods, sets, params, out_path, template_path):
    """Prints data calculated from sim output file to optimization model input file.

    Arguments:
    time_periods - list containing all time periods in output format (values are 1-based times)
    sets - Dict mapping set names to a dict at each time period (0-based time)
    params - Dict mapping param names to the appropriate param dict
    """

    out_strings = []

    t_string = 'set T := '
    for t in time_periods:
        t_string += str(t.one_based_time) + ' '
    t_string += ';'
    out_strings.append(t_string)

    # set SP lists the time periods for the excpetion months within the given time period
    sp_string = 'set SP := '
    for tp in time_periods:
        if (to_month(tp) in exception_months):
            sp_string += str(tp.one_based_time) + ' '
    sp_string += ';'
    out_strings.append(sp_string)

    for st in sets:
        out_strings.append(set_to_string(sets[st]['data'], sets[st]['name']))

    for param in params:
        if (params[param]['name'] == 'vd' or params[param]['name'] == 've'):
            out_strings.append(
                reservoir_param_to_string(params[param]['data'],
                                          params[param]['name']))
        else:
            out_strings.append(
                dict_param_to_string(params[param]['data'],
                                     params[param]['name']))

    # Turn the array of individual strings into a single string that can be printed
    ans = ''
    for string in out_strings:
        ans += string + '\n\n'
    ans = ans[:-2]  # gets rid of the extra 2 newlines at the end

    # copy the template file to out.txt
    copyfile(template_path, out_path)

    # append all the dynamically-generated elements to the copied file
    with open(out_path, 'a') as outfile:
        outfile.write(ans)


def main():
    parser = argparse.ArgumentParser(
        description=
        'Converts Diversion Summary and Reservoir Summary `.rih` files containing flow data into a format which can be used as input for the optimization model used in this project.'
    )

    parser.add_argument('-d', dest='d', metavar='diversion summary path', required=True)
    parser.add_argument('-r', dest='r', metavar='reservoir summary path', required=True)
    parser.add_argument('-s', dest='s', metavar='scenario number', required=True)
    parser.add_argument('-p', dest='p', metavar='penalty sheet path (csv containing demand penalties for each node)')
    parser.add_argument('-l', dest='l', metavar='lookup table path (csv mapping Diversion-summary-ID -> index-within-dataset)')
    parser.add_argument('-o', dest='o', metavar='output path')
    parser.add_argument('-t', dest='t', metavar='template path (file containing elements of output that can be hard-coded)')

    args = parser.parse_args()

    div_sum_path = vars(args)['d']

    res_sum_path = vars(args)['r']

    global scenario
    scenario = vars(args)['s']


    # Generate output file name from input file name, minus the extension
    # This will be the default name unless another is specified
    out_path = div_sum_path.split('.')[0] + '.out'

    if('o' in vars(args)):
        out_path = vars(args)['o']


    # Default penalty sheet location
    penalty_sheet = 'penalties.csv'

    if('p' in vars(args)):
        penalty_sheet = vars(args)['p']


    # Default lookup table location
    lookup_table_path = 'lookup_table.csv'

    if('l' in vars(args)):
        lookup_table_path = vars(args)['l']

    lookup_table = make_lookup_table(lookup_table_path)


    # Default template file location
    template_path = 'final-data-file-template.txt'

    if('t' in vars(args)):
        template_path = vars(args)['t']


    # This is the same data that's contained within set E
    relevant_reservoirs = {
        '0103651': '7.001',
        '0103816': '8.001',
        '0103817': '9.001',
        '6403552': '19.001',
        'zeroes': '24.001',
        '6403551': '33.001',
        '6403906': '44.001'
    }
    #7.001 is Riverside Reservoir 0103651
    #8.001 is Empire Reservoir 0103816
    #9.001 is Jackson Reservoir 0103817
    #19.001 is Prewitt Reservoir 6403552
    #24.001 is Snyder Reservoir Not used, all zeros
    #33.001 is North Sterling Reservoir 6403551
    #44.001 is Julesburg Reservoir 6403906

    penalty_sheet = make_penalty_sheet(penalty_sheet)

    # This is the list of all possible structures, many of which we don't need
    diversion_summary_pages = get_structure_ids(div_sum_path)

    # This is the list of the structure pages we actually care about
    diversion_summary_pages = find_relevant_summary_pages(
        diversion_summary_pages, lookup_table)

    # Get total_short and avail_flow params
    diversion_summary_pages = add_data_to_summary_pages(
        diversion_summary_pages)

    # sort pages in ascending order by index
    diversion_summary_pages = sorted(
        diversion_summary_pages, key=lambda page: page['index'])

    # derive the number of time periods by the number of rows in one of the pages'
    # columns
    # should return 612 in the example case
    num_time_periods = len(diversion_summary_pages[0]['total_short'])

    # create a list of TimePeriod objects for all of the time periods in the data set
    time_periods = []
    for tp in range(num_time_periods):
        # the num_time_periods is 'one-based', because it ends at 612 so it's
        # treating 12 as december and 1 as january
        # doing the range operation shifts this to the range [0,611] which is 0-based time
        # which we can use to create a TimePeriod object
        time_periods.append(TimePeriod(tp))

    # now do the same thing for the reservoir summary
    # TODO abstract this into a function
    reservoir_summary_pages = get_res_sum_pages(res_sum_path)

    reservoir_summary_pages = find_relevant_res_sum_pages(
        reservoir_summary_pages, relevant_reservoirs)

    reservoir_summary_pages = add_data_to_res_sum_pages(
        reservoir_summary_pages, time_periods)

    # In the example file  the reservoir indexes are sorted like numbers, not
    # lexicographically
    reservoir_summary_pages = sorted(
        reservoir_summary_pages, key=lambda page: float(page['index']))

    chat = calc_chat_param(diversion_summary_pages, time_periods)
    de = calc_de_param(diversion_summary_pages, time_periods)

    #print(chat)
    #print(de)

    # Using the chat and de parameters, make the global s and d params
    # That basically just list the nodes with nonzero chat/de params,
    # with a few exceptions
    s = calc_s_set(chat, time_periods)
    d = calc_d_set(de, time_periods)

    # Create the N set
    # N[] = all demand nodes that are in section 64 and hold water rights junior to the compact
    # in other words, the exceptions from the D[] calculation
    n = calc_n_set(time_periods)

    vplus = calc_vplus_param(diversion_summary_pages, d)

    vminus = calc_vminus_param(diversion_summary_pages, s)

    vd = calc_vd_param(reservoir_summary_pages, time_periods)

    ve = calc_ve_param(reservoir_summary_pages, time_periods)

    # Should print the penalties for each de value
    pe = calc_pe_param(penalty_sheet, de)

    sets = {
        's': {
            'name': 'S',
            'data': s
        },
        'd': {
            'name': 'D',
            'data': d
        },
        'n': {
            'name': 'N',
            'data': n
        }
    }

    params = {
        'chat': {
            'name': 'chat',
            'data': chat
        },
        'de': {
            'name': 'de',
            'data': de
        },

        #at time t, the volume of river water currently entering demand node d for each scenario O (Diversion Summary column 25, "River Inflow") (AF)
        'vplus': {
            'name': 'vplus',
            'data': vplus
        },

        # at time t, volume of river water currently exiting supply node s for each scenario O (Diversion Summary column 28, "River Outflow")(AF)
        'vminus': {
            'name': 'vminus',
            'data': vminus
        },

        #volume of water already diverted into existing reservoir diversion structure e in time t for each scenario O (From Reservoir Operation File, Column 9, "Total Supply") (AF)
        'vd': {
            'name': 'vd',
            'data': vd
        },

        #volume of water already released by existing reservoir e in time t for each scenario O (From Reservoir Operation File, column 13, "Total Release")(AF)
        've': {
            'name': 've',
            'data': ve
        },
        'pe': {
            'name': 'pe',
            'data': pe
        }
    }

    print_to_file(time_periods, sets, params, out_path)


main()
