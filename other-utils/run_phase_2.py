from subprocess import call
import time
from datetime import date
import os

scenario_dir = 'Scenarios/'

out_files_old = [
    [
        ("hist1",	1),
        ("hist2",	2),
        ("mean1",	3),
        ("mean2",	4),
        ("season1", 5),
        ("season2", 6),
    ],
    [
        ("mean3", 1),
        ("season4", 2),
    ],
    [   
        ("season8", 1),
        ("season4", 2),
        ("mean5", 3),
    ],
    [
        ("mean8", 1),
        ("season8", 2),
        ("hist9", 3),
    ],
    [
        ("hist9", 1),
        ("mean8", 2),
    ],
    [
        ("season9", 1),
        ("mean6", 2),
    ],
    [
        ("hist6", 1),
        ("hist4", 2),
    ],
    [
        ("hist10", 1),
        ("hist5", 2),
        ("season10", 3),
    ],
    [
        ("hist10", 1),
    ],
    [
        ("hist4", 1),
        ("mean5", 2),
        ("mean7", 3),
    ],
    [
        ("mean6", 1),
    ],
    [
        ("season5", 1),
        ("hist7", 2),
        ("hist3", 3),
    ],
    [
        ("hist5", 1),
        ("season7", 2),
    ],
    [
        ("mean4", 1),
        ("season7", 2),
    ],
    [
        ("season5", 1),
        ("season9", 2),
        ("mean10", 3),
    ],
    [
        ("season3", 1),
        ("mean10", 2),
        ("hist8", 3),
    ],
    [
        ("mean4", 1),
    ],
    [
        ("hist6", 1),
        ("season6", 2),
        ("mean9", 3),
    ],
    [
        ("hist8", 1),
    ],
    [
        ("season6", 1),
        ("mean7", 2),
        ("hist7", 3),
    ],
    [
        ("hist3", 1),
        ("season10", 2),
    ]
]

out_files = [
    [
        ('hist3', 1),
        ('hist4', 2),
        ('mean3', 3),
        ('mean4', 4),
        ('season3', 5),
        ('season4', 6)
    ],
    [
        ('hist5', 1),
        ('hist6', 2),
        ('hist7', 3),
        ('hist8', 4),
        ('hist9', 5),
        ('hist10', 6)
    ],
    [
        ('mean5', 1),
        ('mean6', 2),
        ('mean7', 3),
        ('mean8', 4),
        ('mean9', 5),
        ('mean10', 6)
    ],
    [
        ('season5', 1),
        ('season6', 2),
        ('season7', 3),
        ('season8', 4),
        ('season9', 5),
        ('season10', 6)
    ]    
]
                                	
def run_conversion():
    conversion_dir = f'{scenario_dir}conversion_{date.isoformat(date.today())}_{time.strftime("%H-%M-%S")}/'
    os.mkdir(conversion_dir)
    for index, block in enumerate(out_files):
        # each block contains a number of scenarios, each one of which needs its own output file
        # they are each numbered with scenario numbers relative to their block
        # each block will be put in a separate directory, known by block number
        block_dir = f'{conversion_dir}block_{index}/'
        os.mkdir(block_dir)
        print(f'CREATING BLOCK: {index}')
        for scenario in block:
            scenario_name = scenario[0]
            scenario_num = scenario[1]
            div_name = f'{scenario_dir}{scenario_name}.xdd'
            res_name = f'{scenario_dir}{scenario_name}.xre'
            out_path = f'{block_dir}{scenario_name}.out'
            print('\tConverting', div_name, '...', end='', flush=True)
            call(['python3', 'sim_output_to_opt_input.py', '-d', div_name, '-r', res_name, '-s', str(scenario_num), '-o', out_path])
            print('Done.')
        print()

run_conversion()
