# Guide to working with this stuff

## Parsing: Diversion Summary

### Regular expression for pages of diversion summary:

`PAGE NO.\s\+\d\+`

Each page is a different structure, but not necessarily a node

Within each page it is grouped up by year, and within each year it is broken down into months


### To find a structure by a certain ID:

`STRUCTURE ID.*<id>\s`


## QUESTIONS:

- Does each time period represent 1 month? I'm pretty sure it does, but just to make sure
Yep
Make sure program can handle arbitrary number of time periods

- There are a whole lot more pages on the diversion summary (~301 I think?) than there are nodes represented in the excel spreadsheet (49)
Go through all the pages on the diversion summary, if there is no match found for it in the excel sheet, drop it and move on

- Why are there several nodes in the excel sheet with no data attached to them
Ignore that, fix it later when you do your own parsing

- Why is node 6 listed as in supply during time period 14 when its available flow is the same as in the previous time period? Time period 48 makes sense, as during that time period node 5 has 49217 as its available flow, and then 6 has 49717. So since it increased 5 -> 6, it makes sense that 6 should be in supply at that time. However, at time period 14, they are both the same. So why is 6 in supply?
It shouldn't be, your calculations are correct

- What/where is node '1'? On the excel spreadsheet, it has no data associated with it, and it has no ID field either so I don't know where it is in the diversion summary. However, in the final data sample, node 1 is listed as in supply at times. So obviously there is data associated with it somewhere...
Here's an example: at time period 48, 1 is listed as in supply. But there's nothing on the excel sheet under 1 at that time and I can't find it in the diversion summary.
Ab-kersey is "Above kersey:" it is the starting point for our analysis, it represents the flow of the river from the entire river before where we start measuring nodes in-depth -- it is the only baseflow node included, and the only one without an ID because an id doesn't really make any sense for it

PARTIAL ANSWER: Node 1 is ab kersey baseflow -- it has no ID because its ID is simply "baseflow". It is given index 1 in the node list. It is listed as in supply at time period 48 because it has available flow and it is the first in the list, so that can be seen as an increase from the default value of 0. But this raises other questions: are there other baseflow nodes in the excel spreadsheet? In fact just tell me everything about this

- A lot of the structures in the diversion summary cannot be found in the excel spreadsheet (which makes sense because that number has to go from 302 to 49 somehow)

- Is there any pattern to which of the nodes from the diversion summary end up in the excel spreadsheet as nodes with indexes attached to them?

- What should I do with the nodes in the excel sheet that don't have any ID attached to them

- Why is node 21 in Demand at time period 4? When I look at time period 4 for node 21 in the diversion summary, the total short is listed as 0. What is it that determines a node in demand?

ANSWER:
The general rule is that if a node has a total shortage greater than zero, then it is a demand node.
However, during the months of april - october, there are other nodes that will always appear, regardless of whether they have 0 demand, due to some quirks about water rights and the fact that their demand must be filled after those of the state of nebraska
Sometimes, the supply node list will be longer during that time period than others -- that's because some of those nodes, the ones that aren't always there, have actual demand via the total short

OMNIPRESENT NODES: 21 41 42 43 44 46 47 48
Demand nodes in section 64 that hold water rights junior to nebraska
