from subprocess import call

scenario_dir = 'MorePhase1/'

def run_conversion(scenario_name):
    for i in range(6,11):
        div_name = scenario_dir + scenario_name + str(i) + '.csv'
        out_name = div_name.split('.')[0] + '.rih'
        print('Converting', div_name, '...', end='')

        call(['python3', 'csv_to_sim_input.py', div_name, out_name])
        print('Done.')

run_conversion('historical_simulation_')

run_conversion('mean_reduction_simulation_')

run_conversion('seasonal_shift_simulation_')
